## Protocolos para la transmisión de audio y video en Internet

Contenidos de la asignatura PTAVI (Protocolos para la transmisión de audio y video en Internet), del Grado en Ingeniería en Sistemas Audiovisuales y Multimedia de la [Escuela de Ingeniería de Fuenlabrada](https://www.urjc.es/eif) de la [Universidad Rey Juan Carlos](https://urjc.es).

## Materiales de la asignatura

* Código. Programas de ejemplo, soluciones a prácticas, etc. Puedes encontrarlo
  en el [repositorio cursomminet/code de GitLab](https://gitlab.com/cursomminet/code).

* Canal en YouTube.
  Tenemos un [canal en YouTube](https://www.youtube.com/channel/UCSFmAX-r42a3lQAnAhknsqw), donde vamos subiendo las grabaciones de las clases, y otros videos que nos parecen interesantes para seguir la asignatura.
  En particular, están prácticamente todas las clases del curso 2020-2021.

* Transparencias. Estamos colgando las transparencias en el aula virtual.
  Los fuentes de estas transparencias (escritas en LaTeX) están 
  disponibles en el [repositorio cursomminet/ptavi-slides de GitLab](https://gitlab.com/cursomminet/ptavi-slides), donde también puedes consultar los [ficheros PDF correspondientes](https://gitlab.com/cursomminet/ptavi-slides/-/tree/master/pdfs).
  

## Otros materiales recomendados

* Tutorial ["How to Use Python: Your First Steps"](https://realpython.com/python-first-steps/). Breve introducción a los 
  conceptos fundamentales de Python. cubre parte de los contenidos de las
  primeras clases de la asignatura.

* Libro: ["Dive Into Python 3"](https://diveintopython3.net/). Buena referencia para aprender cómo funcionan
  muchos detalles de Python 3. Muy recomendable como complemento a los contenidos de la asignatura,
  en lo que tiene que ver con el lenguaje de programación utilizada. El libro está disponible libremente en línea.

* Guía: ["The Hitchhiker’s Guide to Python!"](https://docs.python-guide.org/). Guía de buenas prácticas
  y detalles interesantes sobre el uso práctico de Python. Buen complemento práctico a los materiales
  anteriores.
