---
title: Acerca de este sitio
subtitle: Qué puedes encontrar aquí
comments: false
---

Este sitio ofrece materiales utilizados en la asignatura PTAVI, impartida en el grado ISAM de la [ETSIT](https://etsit.urjc.es) de la [URJC](https://urjc.es). Hay tanto materiales de estudio, como materiales complementarios.

Si te son útiles, adelante, ¡úsalos!