---
title: Conexion a los laboratorios y PyCharm
subtitle: Comenzando por el principio...
date: 2020-09-29
tags: ["practicas", "labos", "python"]
---

Estas son algunas notas para las sesiones en las que se explica cómo conectarse a los laboratorios, y cómo ejecutar PyCharm.

# Acceso remoto a los laboratorios

Primero, recordad crear o renovar cuenta según las instrucciones del sitio web de los [laboratorios Linux](https://labs.etsit.urjc.es). Las instrucciones sobre cómo conectarse están accesibles desde el menú "tutoriales" de ese mismo sitio:

* [Acceso via ssh desde Linux](https://labs.etsit.urjc.es/index.php/tutoriales/abrir-una-sesion-ssh-en-el-laboratorio-desde-linux-unix/)
* [Acceso via ssh desde Windows](https://labs.etsit.urjc.es/index.php/tutoriales/abrir-una-sesion-ssh-en-el-laboratorio-desde-windows-2/)

Os aconsejamos que preparéis el acceso mediante par de claves ([tutorial con instrucciones](https://www.howtogeek.com/424510/how-to-create-and-install-ssh-keys-from-the-linux-shell/), [detalles sobre cómo funcionan las claves ssh](https://www.ssh.com/ssh/key/)).

* [Acceso via WebVNC](https://labs.etsit.urjc.es/index.php/tutoriales/abrir-una-sesion-grafica-vnc-a-traves-del-navegador/). El punto de acceso es [https://labs.etsit.urjc.es/vnc/](https://labs.etsit.urjc.es/vnc/). Antes de acceder por primera vez tendrás que haber usado `vncpasswd`, conectándote vía ssh, para crear una contraseña para VNC.

# Para lanzar PyCharm

Cuando se ejecuta por primera vez, se hace una configuración mínima. Entre otras cosas, permite crear un script, `bin/charm`, dentro de tu directorio hogar, que podrás ejecutar después desde la consola para ver cualquier fichero. Si quieres que te funcione cómodamente, tendrás que añadir texto a tus fichero `.profile` el siguiente texto (si no está ya):

```
set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi
```
