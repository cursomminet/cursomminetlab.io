# Capturas que se pueden usar en clase

* sip-rtp-gsm.pcap

https://wiki.wireshark.org/SampleCaptures#SIP_and_RTP

Comunicación completa entre dos agentes, unidireccional, sólo hay audio, utiliza SIP, RTP y GSM (como codec de audio).

* MagicJack+_short_call.pcap

https://wiki.wireshark.org/SampleCaptures#SIP_and_RTP

Ejemplo completo de llamada telefónica

