## Licenses

* [Earthrise from lunar orbit (Apollo 11)](earth-rising.ogx)
  * [Obtained from Internet Archive](https://archive.org/details/EarthriseFromLunarOrbitapollo11)
  * Author: Michael Collins (original images), NASA (compilation)
  * License: [Creative Commons Public Domain Mark 1.0](http://creativecommons.org/publicdomain/mark/1.0/)